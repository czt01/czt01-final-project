# Dictionary - Backend

## How to build the application

```mvn clean package```

## How to run locally:
```mvn clean package```

THEN

```java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -Dspring.profiles.active=local -jar ./target/dictionary-service-0.0.1-SNAPSHOT.jar```

OR

```java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -Dspring.profiles.active=local -jar target/dictionary-service-0.0.1-SNAPSHOT.jar```

