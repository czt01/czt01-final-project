# Dictionary - Frontend

# Dictionary - Backend

As a customer I would like to have an application where people can collect pairs of words in two different languages by topics.
The user can register and login to the application.

Happy path:
1. I can create a new topic or choose an existing one.
2. For the chosen topic I can create a new sheet where I can collect pairs or I can continue editing an existing sheet.
3. I can add/remove labels (eg, HU, EN, computer science) to my sheet from a predefined table.
4. If I want I can share my sheets with anyone registered in the application.

Roles of application:
1. USER
2. ADMIN

Only ADMINs can maintain the labels.

## Tables
WordPairs:
- id
- leftValue
- rightValue
- sheetId

Sheet:
- id
- name
- description
- List Of WordPairs
- topicId
- List Of Labels

Label:
- id
- name

Topic:
- id
- name
- description
- List Of Sheets

# Coding Style
1. We use the default settings of IntelliJ without allowing star imports, containing but not exclusively
   1. Indentations
   2. Whitespaces
   3. Statements
   4. Blocks

# Coding Convention - Source code
1. This part does not contain the exceptions for tests.
2. Follow the rules of:
    1. OOP principles
         1. Abstraction
         2. Encapsulation
         3. Polymorphism
         4. Inheritance
    2. SOLID
         1. Single Responsibility Principle
         2. Open-Closed Principle
            1. Note: Most of the time, using switch with enum values breaks this rule.
         3. Liskov Substitution Principle
         4. Interface Segregation Principle
         5. Dependency Inversion Principle
    3. DRY (Do not repeat yourself.)
    4. KISS (Keep it simple, stupid.)
    5. Boy scout rule. Leave the campground cleaner than you found it.
    6. Prefer composition to inheritance.
    7. Use/Create beans wherever you can.
    8. No magic numbers; used named, static final variables.
    9. Use Lombok annotations.
    10. Law of Demeter. (A class should know only its direct dependencies.)
    11. Prefer polymorphism to if/else or switch/case.
    12. Use dependency injections where it is possible.
    13. Do not handle any code changes going to master as a temporary solution from quality point of view. 

## Imports
1. No star imports.
2. No static imports.

## Logging
1. WARN 
   1. The application can serve the request, but some default values are applied as a safe case or ignorable exceptions happen.
2. ERROR 
   1. Application / Flow is crashed, so the response is cannot be generated.
   2. Error log statements must express the context (entity type, id, operation, error message)

## Comments
1. In general, comments should be avoided. Think twice before adding any comments!
2. Comments are accepted for 
   1. those interfaces what are going to be given/used to/by another teams,
   2. classes / methods encapsulates complex algorithms / formulas,
   3. unusual situations / unexpected decisions where the reasons marked.
3. TODOs without a ticket number cannot be merged to master. The author and a ticket number must be put in TODO.
   1. eg, TODO (pal) LPS-1678, description
4. Do not be redundant. Do not add obvious noise.
5. Do not comment out code. Just remove it.

## Naming
1. Names should be self-explanatory, meaningful, pronounceable, searchable.
2. Do not append prefixes or type information.
   1. Note: Using type suffix is allowed when the same information is represented with different data types in a method, eg Optional, Mono, Flux
3. Do not use any shorter form of an expression. Do not use abbreviations belonging to the business domain.
4. Use consistent verbs per concept.

## Variables
1. Do not use ```var```.
2. Avoid calling methods multiple times when their return values are needed. 
   Assign the return value to a variable when a method's return value is used multiple times.
3. Optional must be checked before getting the value.
    1. Note: if the intention is to throw exceptions when an Optional is empty, throw a business specific exception which can easily describe/explain the situation with expectations.

## Methods
1. Maximum number of method parameters: 3 ( - 5)
   1. Exception: constructors.
2. Maximum length of a method: 40 rows.
3. Do not assign to parameters. (Parameters are final / effectively final.)
4. A method should be small and focused on one task.
5. Split a method into several methods if it operates on different levels of data.
6. Return early on the shortest path when possible to decrease the complexity.
7. A method should not change the state of any parameters (eg adding a new item to a list) unless the method name clearly states it.
8. A method should not do anything which is not expected from its name.
9. Use ```@Override``` when overriding is applied.
10. Do not use flag arguments.
11. Avoid using Optional parameters.
12. The visibility of a method can be changed for tests.

## Classes / Interfaces
1. Do not use utility classes. Solutions:
   1. Create a class to wrap the data and the methods together, eg Decorator Design Pattern, Wrapper Classes.
   2. Define the methods where they are used.
2. Do not use ```default``` methods except mapper-like and functional interfaces.
3. Generate constructors with Lombok except in beans.
4. Maximum number of instance variables / dependencies: 6
5. Minimize the public and protected interface.
   1. Note: The visibility of a method can be changed for tests (to default or protected).
6. Use Instant to represent date.
    1. Note: Use ISO 8601 format when serializing date to DB or JSON.
7. Entity equals should rely on only its ID field(s).
8. Prefer immutable classes.
9. Use enums or constant class (final class with private constructors) instead of constant interface.
10. Use interface references to collections, eg List for ArrayList.
11. A class should contain less than 300 rows.
   1. Exception: tests.
12. Put ```@FunctionalInterfaces``` to a functional interface.
13. Hide internal data structure.
   1. Note: It is an approach what we can see for example in the ArrayList. It hides the array used behind itself totally.
   2. Example: Having a class which contains a List. We should define the add() method in the class to insert a new item into the list instead of getting the list and inserting directly.
14. Methods should be in the right class. The methods of a class should be interested in the variables and functions
of the class they belong to, and not the variables and functions of other
classes. When a method uses accessors and mutators of some other object
to manipulate the data within that object, then it envies the scope of the
class of that other object. It wishes that it were inside that other class so
that it could have direct access to the variables it is manipulating.
15. Avoid defining interfaces only containing getters / setters. Solution:
   1. Abstract classes containing the attributes
   2. Normal classes containing the attributes
    
16. Mappers must be separated, 1 mapper is for converting type A to type B, and type B to type A


## Dependencies
1. Do not introduce any new 3rd party libraries as dependencies if the work can be done by using the existing frameworks.

## Application layers

### Controllers
1. It must not contain any business logic.
2. It must not contain any repository related code parts.
3. Mapping can be places here only if it happens between an internal representation and a client representation.
4. It must be tested with real requests.

### Services
1. All business implementations are placed in this layer.
2. Never include infrastructure related components.
   1. Note: The core business logic should not contain any infrastructure related components. 
      Infrastructure/messaging/rest... should be around the business logic and not in it.
3. It must be fully unit tested.

### Repositories
1. It must not contain any business logic.

# Coding Convention - Tests
1. Everything should be applied from "Coding Convention - Source code" with the changes described in this section.
2. Ensure that test code is separated from production code (source sets: main/test/...).
3. Follow the rules of:
   1. FIRST
      1. Fast
      2. Independent
      3. Repeatable
      4. Self-validating
      5. Timely
4. Used frameworks:
    1. JUnit5
    2. Mockito
    3. WireMock
5. After every change, the code coverage cannot be less than it was before the change.
6. If a feature contains repository related actions, it must be tested with the real repository layer (too) on higher level.

## Imports
1. Static import must be used for 
   1. Mockito,
   2. Assertions.

## Comments
1. Test methods should contain Given / When / Then as comments to increase readability.

## Variables
1. The object being tested should be named as underTest, eg having a Calculator class, the tested variable of Calculator should be ```Calculator underTest;```

## Test methods
1. Naming examples: shouldXReturnYWhenZ (ShouldExpectedBehaviorWhenStateUnderTest).
2. Test methods should test only one thing on the right level.
3. Use the most appropriate assertion methods, eg
   1. assert(Not)Null() for checking null / not null values
   2. assertEquals() for object comparison
   3. assertTrue() / assertFalse() for checking logical values 
   4. assertAll() for checking a subset of attributes of a given object
      1. Note: This is optional
4. Consider using @DisplayName for increasing readability in parameterised tests. 

## Test levels

### Unit tests
1. Every method containing business critical parts must be unit tested.
2. Try to avoid using the real implementations of the dependencies. Use mocks, fake objects wherever possible.
   1. Note: Spies can be used for simple cases (consider complexity). Maximum length of chain of dependencies is 1, eg 
      1. A depends on B: A can be tested with real B, B must be unit tested fully with mocks
      2. A depends on B, B depends on C: A can be tested with real B, but C must be a mock, B can be tested with real C, C must be fully unit tested with mocks
      3. A depends on, B, C, D: A cannot be tested with real B, real C and real D because it would be not a unit test anymore. It is a higher level test.
3. Test only one code unit at a time.
4. Unit tests must be placed into the same package as the tested class is in.

### Service / Feature tests (Micronaut tests)
1. Mock out all external services and dependencies.
2. For controllers, implement micronaut tests where real requests are used

### Integration tests (e2e tests of the application)
1. Mock out all external services and dependencies.
2. Use TDD & BDD.
3. Use the application.yml file defined for the production env to do testing on configurations.
   1. Exception: When something is not configured in application.yml, eg a feature is only enabled in PPE.

# Code reviews
1. Use Internal Quality Bar all the time of checking a PR.
2. A PR cannot be merged if an approval missing from any change requester.
3. Every change must have a PR.
4. If a new rule (coding conventions) discussed and accepted regarding a PR that new rule must be applied in the given PR too. So the PR cannot be merged to master without the changes required by the new rule.
5. At least 2 approvals are needed to merge a PR.
   1. All change requester.
   2. At least 1 from the 'online team' and at least 1 from the 'till team'.
6. PR must be merged by the author.    

# Tools
1. Checkstyle
      1. Never change the content in the settings by yourself. Every change must be agreed on team level.
2. FindBugs / SpotBugs
3. Jacoco

# Refactor
1. Refactor the code when you find any technical debt / code smells such as
   1. Rigidity. The software is difficult to change. A small change causes a cascade of subsequent changes.
   2. Fragility. The software breaks in many places due to a single change.
   3. Immobility. You cannot reuse parts of the code in other projects because of involved risks and high effort.
   4. Needless Complexity.
   5. Needless Repetition.
   6. Opacity. The code is hard to understand.
2. Act when you find bad practises:
   1. Do the refactor in the touched files if the change is small / effecting few classes / does not take more than an hour. 
      A business related change should not contain more than 20% of technical refactors. (Create a separated PR whenever reasonable.)
   2. Create a new ticket with a proper technical description and put it into the backlog if the previous rules cannot be applied. 
      The ticket must contain 'Why?' and 'What?'.
   3. Mark non-trivial refactors with comments in a PR to make the changes more understandable.   
   
# Branching
   1. All change must be on a feature branch. 
      Its name must be like [feature|bugfix|hotfix]/{Jira Ticket Number}-{shortDescription}, eg ```feature/LPS-1625-creatingStyleGuide```
      Use the defined hooks to have a name validation.
   2. All commit messages must include the Jira ticket number.
   3. Main branch name: ```master```
   4. Nothing can go into ```master``` without having a proper PR for it and review on it.

# Summary
The end game is to write code that makes the life of future authors and maintainers easy. 
If something is strange for most of the developers, do not do it. 

Do not try to play smarter. Just follow the basics.
