package hu.czirjaktech.dictionaryservice.mapper;

import hu.czirjaktech.dictionaryservice.dto.Role;
import hu.czirjaktech.dictionaryservice.dto.UserDto;
import hu.czirjaktech.dictionaryservice.repository.user.User;
import org.junit.jupiter.api.Test;
import static hu.czirjaktech.dictionaryservice.dto.Role.ROLE_USER;
import static org.junit.jupiter.api.Assertions.*;

class UserMapperTest {

   private UserMapper underTest = new UserMapper();

   @Test
   void testUserFromEntityToDto(){
       //Given
       Long id = 1L;
        String username = "username";
        String password = "password";
        Role role = ROLE_USER;

       User input = User.builder()
               .id(id)
               .active(true)
               .password(password)
               .role(role)
               .username(username)
               .build();

       UserDto expectedResult = UserDto.builder()
               .id(id)
               .active(true)
               .password(password)
               .role(role)
               .username(username)
               .build();

       //When
        UserDto result = underTest.convertToDto(input);

       //Then
       assertEquals(result, expectedResult);
   }

   @Test
    void testUserFromDtoToEntity(){
        //Given
        Long id = 1L;
        String username = "username";
        String password = "password";
        Role role = ROLE_USER;

        UserDto input = UserDto.builder()
                .id(id)
                .active(true)
                .password(password)
                .role(role)
                .username(username)
                .build();

        User expectedResult = User.builder()
                .id(id)
                .active(true)
                .password(password)
                .role(role)
                .username(username)
                .build();

        //When
        User result = underTest.convertToEntity(input);

        //Then
       assertAll(
                ()-> assertEquals(result.getId(), expectedResult.getId()),
                ()-> assertEquals(result.getPassword(), expectedResult.getPassword()),
                ()-> assertEquals(result.getRole(), expectedResult.getRole()),
                ()-> assertEquals(result.getUsername(), expectedResult.getUsername()),
                ()-> assertEquals(result.isActive(), expectedResult.isActive())
       );

    }
}