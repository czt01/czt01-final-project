package hu.czirjaktech.dictionaryservice.mapper;

import hu.czirjaktech.dictionaryservice.dto.LabelDto;
import hu.czirjaktech.dictionaryservice.repository.label.Label;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class LabelMapperTest {

    private LabelMapper underTest;

    @BeforeEach
    void init() {
        underTest = new LabelMapper();
    }

    @Test
    void shouldReturnLabelDto() {
        //Given

        Long id = 1L;
        String name = "name";

        Label input = Label
                .builder()
                .id(id)
                .name(name)
                .build();

        LabelDto expectedResult = LabelDto
                .builder()
                .id(id)
                .name(name)
                .build();

        //When
        LabelDto result = underTest.convertToDto(input);

        //Then
        assertEquals(expectedResult, result);
    }

    @Test
    void shouldReturnLabelEntity() {
        //Given
        Long id = 1L;
        String name = "name";

        LabelDto input = LabelDto
                .builder()
                .id(id)
                .name(name)
                .build();

        Label expectedResult = Label
                .builder()
                .id(id)
                .name(name)
                .build();

        //When
        Label result = underTest.convertToEntity(input);

        //Then
        assertAll(
                () -> assertEquals(expectedResult.getId(), result.getId()),
                () -> assertEquals(expectedResult.getName(), result.getName())
        );

    }
}