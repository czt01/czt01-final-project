package hu.czirjaktech.dictionaryservice.mapper;

import hu.czirjaktech.dictionaryservice.dto.LabelDto;
import hu.czirjaktech.dictionaryservice.dto.SheetDto;
import hu.czirjaktech.dictionaryservice.dto.WordPairDto;
import hu.czirjaktech.dictionaryservice.repository.label.Label;
import hu.czirjaktech.dictionaryservice.repository.sheet.Sheet;
import hu.czirjaktech.dictionaryservice.repository.wordpair.WordPair;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class SheetMapperTest {

    private static final Label LABEL = Mockito.mock(Label.class);
    private static final LabelDto LABEL_DTO = Mockito.mock(LabelDto.class);
    private static final WordPairDto WORD_PAIR_DTO = Mockito.mock(WordPairDto.class);
    private static final WordPair WORD_PAIR = Mockito.mock(WordPair.class);

    @Mock
    private LabelMapper labelMapper;

    @Mock
    private WordPairMapper wordPairMapper;

    @InjectMocks
    private SheetMapper underTest;

    @Test
    public void testMapperFromEntityToDto(){
        //Given
        String name = "name";
        Long id = 1L;
        String description = "description";
        Mockito.when(labelMapper.convertToDto(LABEL)).thenReturn(LABEL_DTO);
        Mockito.when(wordPairMapper.convertToDto(WORD_PAIR)).thenReturn(WORD_PAIR_DTO);

        Sheet input = Sheet.builder()
                .wordPairs(List.of(WORD_PAIR))
                .labels(Set.of(LABEL))
                .name(name)
                .description(description)
                .id(id)
                .build();

        SheetDto expectedResult = SheetDto.builder()
                .wordPairs(List.of(WORD_PAIR_DTO))
                .labels(Set.of(LABEL_DTO))
                .name(name)
                .description(description)
                .id(id)
                .build();

        //When
        SheetDto result = underTest.convertToDto(input);

        //Then
        assertEquals(expectedResult, result);
    }

    @Test
    void testMapperFromDtoToEntity(){
        //Given
        String name = "name";
        Long id = 1L;
        String description = "description";
        Mockito.when(labelMapper.convertToEntity(LABEL_DTO)).thenReturn(LABEL);
        Mockito.when(wordPairMapper.convertToEntity(WORD_PAIR_DTO)).thenReturn(WORD_PAIR);

        SheetDto input = SheetDto.builder()
                .wordPairs(List.of(WORD_PAIR_DTO))
                .labels(Set.of(LABEL_DTO))
                .name(name)
                .description(description)
                .id(id)
                .build();

        Sheet expectedResult = Sheet.builder()
                .wordPairs(List.of(WORD_PAIR))
                .labels(Set.of(LABEL))
                .name(name)
                .description(description)
                .id(id)
                .build();
        //When

        Sheet result = underTest.convertToEntity(input);
        //Then

        assertAll(
                ()->assertEquals(expectedResult.getId(), result.getId()),
                ()->assertEquals(expectedResult.getDescription(), result.getDescription()),
                ()->assertEquals(expectedResult.getLabels(), result.getLabels()),
                ()->assertEquals(expectedResult.getWordPairs(), result.getWordPairs()),
                ()->assertEquals(expectedResult.getName(), result.getName())
        );
    }
}