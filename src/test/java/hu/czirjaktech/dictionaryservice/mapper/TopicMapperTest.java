package hu.czirjaktech.dictionaryservice.mapper;

import hu.czirjaktech.dictionaryservice.dto.TopicDto;
import hu.czirjaktech.dictionaryservice.repository.topic.Topic;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TopicMapperTest {

    private TopicMapper underTest;

    @BeforeEach
    void init() {
        underTest = new TopicMapper();
    }

    @Test
    void shouldTopicMappedToTopicDto(){
        //Given
        Long id = 0L;
        String name = "name";
        String description = "description";

        Topic input = Topic
                .builder()
                .id(id)
                .name(name)
                .description(description)
                .build();

        TopicDto expectedResult = TopicDto
                .builder()
                .id(id)
                .name(name)
                .description(description)
                .build();

        //When
        TopicDto result = underTest.convertToDto(input);

        //Then
        assertEquals(expectedResult, result);
    }

    @Test
    void shouldTopicDtoMappedToTopic(){
        //Given
        Long id = 0L;
        String name = "name";
        String description = "description";

        TopicDto input = TopicDto
                .builder()
                .id(id)
                .name(name)
                .description(description)
                .build();

        Topic expectedResult = Topic
                .builder()
                .id(id)
                .name(name)
                .description(description)
                .build();

        //When
        Topic result = underTest.convertToEntity(input);

        //Then
        assertAll(
                () -> assertEquals(expectedResult.getId(), result.getId()),
                () -> assertEquals(expectedResult.getName(), result.getName()),
                () -> assertEquals(expectedResult.getDescription(), result.getDescription())
        );
    }

}