package hu.czirjaktech.dictionaryservice.mapper;

import hu.czirjaktech.dictionaryservice.dto.SheetDto;
import hu.czirjaktech.dictionaryservice.dto.WordPairDto;
import hu.czirjaktech.dictionaryservice.repository.wordpair.WordPair;
import hu.czirjaktech.dictionaryservice.repository.sheet.Sheet;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WordPairMapperTest {

    @InjectMocks
    WordPairMapper underTest;

    @Test
    public void testMapperFromDtoToEntity(){
        //Given
        Long id = 1L;
        String leftValue = "left";
        String rightValue = "right";

        WordPairDto input = WordPairDto.builder()
                .id(id)
                .rightValue(rightValue)
                .leftValue(leftValue)
                .build();

        WordPair expectedResult = WordPair.builder()
                .id(id)
                .rightValue(rightValue)
                .leftValue(leftValue)
                .build();

        //When
        WordPair result = underTest.convertToEntity(input);

        //Then
        assertEquals(result,expectedResult);
    }

    @Test
    public void testMapperFromEntityToDto(){
        //Given
        Long id = 1L;
        String leftValue = "left";
        String rightValue = "right";

        WordPair input = WordPair.builder()
                .id(id)
                .rightValue(rightValue)
                .leftValue(leftValue)
                .build();

        WordPairDto expectedResult = WordPairDto.builder()
                .id(id)
                .rightValue(rightValue)
                .leftValue(leftValue)
                .build();

        //When
       WordPairDto result = underTest.convertToDto(input);

        //Then
        assertAll(
                ()-> assertEquals(result.getId(),expectedResult.getId()),
                ()-> assertEquals(result.getLeftValue(),expectedResult.getLeftValue()),
                ()-> assertEquals(result.getRightValue(),expectedResult.getRightValue())
        );
    }
}