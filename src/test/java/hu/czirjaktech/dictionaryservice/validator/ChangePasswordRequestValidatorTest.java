package hu.czirjaktech.dictionaryservice.validator;

import hu.czirjaktech.dictionaryservice.dto.request.ChangePasswordRequest;
import org.junit.jupiter.api.Test;

import javax.validation.ValidationException;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ChangePasswordRequestValidatorTest {

    ChangePasswordRequestValidator underTest = new ChangePasswordRequestValidator();

    @Test
    public void shouldNotThrowingException(){
        //Given
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
        changePasswordRequest.setCurrentPassword("Dummy123");
        changePasswordRequest.setNewPassword("NewDummy123");
        changePasswordRequest.setConfirmNewPassword("NewDummy123");

        //When - Then not throwing Exception
        underTest.validate(changePasswordRequest);

    }

    @Test
    public void shouldThrowingExceptionWhenPasswordsNotMatching(){
        //Given
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
        changePasswordRequest.setCurrentPassword("Dummy123");
        changePasswordRequest.setNewPassword("NewDummy123");
        changePasswordRequest.setConfirmNewPassword("NewDummy1234");

        //When-Then
        assertThrows(ValidationException.class, () -> underTest.validate(changePasswordRequest));
    }
}