package hu.czirjaktech.dictionaryservice.validator;

import hu.czirjaktech.dictionaryservice.dto.request.RegistrationRequest;
import org.junit.jupiter.api.Test;

import javax.validation.ValidationException;

import static org.junit.jupiter.api.Assertions.assertThrows;

class RegistrationRequestValidatorTest {

    RegistrationRequestValidator underTest = new RegistrationRequestValidator();

    @Test
    void shouldNotThrowingException(){
        //Given
        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setPassword("Dummy123");
        registrationRequest.setPasswordAgain("Dummy123");
        registrationRequest.setUsername("dummy@dummy.com");

        //When - No Exception
        underTest.validate(registrationRequest);
    }

    @Test
    void shouldThrowExceptionWhenPasswordsNotMatching(){
        //Given
        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setPassword("Dummy123");
        registrationRequest.setPasswordAgain("Dummy12345");
        registrationRequest.setUsername("dummy@dummy.com");

        //When - Then
        assertThrows(ValidationException.class, ()-> underTest.validate(registrationRequest));
    }
}