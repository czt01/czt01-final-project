package hu.czirjaktech.dictionaryservice.service;

import hu.czirjaktech.dictionaryservice.dto.WordPairDto;
import hu.czirjaktech.dictionaryservice.exception.DataNotFoundException;
import hu.czirjaktech.dictionaryservice.mapper.WordPairMapper;
import hu.czirjaktech.dictionaryservice.repository.wordpair.WordPair;
import hu.czirjaktech.dictionaryservice.repository.wordpair.WordPairRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class WordpairServiceTest {
    @Mock
    private WordPairRepository wordPairRepository;
    @Mock
    private WordPairMapper wordPairMapper;
    @InjectMocks
    private WordpairService underTest;

//    @Test
//    void testSave() {
//        // GIVEN
//        WordPairDto expectedResult = WordPairDto.builder().build();
//
//        WordPair wordPair = WordPair.builder().build();
//
//        when(wordPairMapper.convertToEntity(expectedResult)).thenReturn(wordPair);
//        when(wordPairRepository.save(wordPair)).thenReturn(wordPair);
//        when(wordPairMapper.convertToDto(wordPair)).thenReturn(expectedResult);
//
//        // WHEN
//        WordPairDto result = underTest.save(expectedResult);
//
//        // THEN
//        assertEquals(expectedResult, result);
//        verify(wordPairMapper).convertToEntity(expectedResult);
//        verify(wordPairRepository).save(wordPair);
//        verify(wordPairMapper).convertToDto(wordPair);
//    }

    @Test
    void testUpdateWithFoundObject() {
        // GIVEN
        WordPairDto expectedResult = WordPairDto.builder().build();
        WordPairDto wordPairDto = WordPairDto.builder().build();
        WordPair wordPair = WordPair.builder().build();

        when(wordPairRepository.findById(wordPairDto.getId())).thenReturn(Optional.of(wordPair));
        doNothing().when(wordPairMapper).updateEntity(wordPair, wordPairDto);
        when(wordPairMapper.convertToDto(wordPair)).thenReturn(wordPairDto);
        when(wordPairRepository.save(wordPair)).thenReturn(wordPair);

        // WHEN
        WordPairDto result = underTest.update(wordPairDto);

        // THEN
        assertEquals(expectedResult, result);
        verify(wordPairMapper).updateEntity(wordPair, wordPairDto);
    }

    @Test
    void testUpdateWithoutObject() {
        // GIVEN
        WordPairDto wordPairDto = WordPairDto.builder().build();

        when(wordPairRepository.findById(wordPairDto.getId())).thenReturn(Optional.empty());

        // WHEN

        // THEN
        assertThrows(DataNotFoundException.class, () -> underTest.update(wordPairDto));
    }

    @Test
    void shouldFindAllRetrieveAllItems() {
        // GIVEN
        WordPairDto wordPairDto = WordPairDto.builder().build();
        WordPair wordPair = WordPair.builder().build();

        when(wordPairRepository.findAll()).thenReturn(List.of(wordPair));
        when(wordPairMapper.convertToDto(wordPair)).thenReturn(wordPairDto);

        List<WordPairDto> expectedResult = List.of(wordPairDto);

        // WHEN
        List<WordPairDto> result = underTest.findAll();

        // THEN
        assertEquals(expectedResult, result);
    }

    @Test
    void testDeleteById() {
        // GIVEN
        Long id = 1L;

        doNothing().when(wordPairRepository).deleteById(id);

        // WHEN
        underTest.deleteById(id);

        // THEN
        verify(wordPairRepository).deleteById(id);
    }

    @Test
    void testDeleteAll() {
        // GIVEN
        doNothing().when(wordPairRepository).deleteAll();

        // WHEN - THEN
        underTest.deleteAll();
        verify(wordPairRepository).deleteAll();
    }
}