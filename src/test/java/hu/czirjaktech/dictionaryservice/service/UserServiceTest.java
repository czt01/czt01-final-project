package hu.czirjaktech.dictionaryservice.service;

import hu.czirjaktech.dictionaryservice.dto.UserDto;
import hu.czirjaktech.dictionaryservice.exception.DataNotFoundException;
import hu.czirjaktech.dictionaryservice.mapper.UserMapper;
import hu.czirjaktech.dictionaryservice.repository.user.User;
import hu.czirjaktech.dictionaryservice.repository.user.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserService underTest;

    @Test
    void shouldSetUserInactive() {
        //given
        Long input = 1L;
        User originalUser = User.builder()
                .id(input)
                .username("username")
                .active(true)
                .password("password")
                .build();
        User updatedUser = User.builder()
                .id(input)
                .username("username")
                .active(false)
                .password("password")
                .build();
        when(userRepository.findById(input)).thenReturn(Optional.of(originalUser));

        //when
        UserDto result = underTest.makeInactive(input);

        //then
        verify(userRepository).findById(input);
        verify(userRepository).save(updatedUser);
        verify(userMapper).convertToDto(any());

    }

    @Test
    void shouldThrowExceptionWhenUserNotFound() {
        //given
        Long input = 1L;
        when(userRepository.findById(input)).thenReturn(Optional.empty());

        //when-then
        assertThrows(DataNotFoundException.class, () -> underTest.makeInactive(input));
        verify(userRepository).findById(input);
        verify(userRepository, times(0)).save(any());
        verify(userMapper,times(0)).convertToDto(any());
    }
}