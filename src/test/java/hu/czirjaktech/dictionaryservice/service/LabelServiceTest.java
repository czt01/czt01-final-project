package hu.czirjaktech.dictionaryservice.service;

import hu.czirjaktech.dictionaryservice.dto.LabelDto;
import hu.czirjaktech.dictionaryservice.exception.DataNotFoundException;
import hu.czirjaktech.dictionaryservice.mapper.LabelMapper;
import hu.czirjaktech.dictionaryservice.repository.label.Label;
import hu.czirjaktech.dictionaryservice.repository.label.LabelRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LabelServiceTest {
    @Mock
    private LabelRepository labelRepository;
    @Mock
    private LabelMapper labelMapper;
    @InjectMocks
    private LabelService underTest;

    @Test
    void testDeleteAll() {
        //GIVEN
        doNothing().when(labelRepository).deleteAll();

        //WHEN
        underTest.deleteAll();

        //THEN
        verify(labelRepository).deleteAll();
    }

    @Test
    void testDeleteById() {
        //GIVEN
        Long dummyId = 1L;

        doNothing().when(labelRepository).deleteById(dummyId);

        //WHEN
        underTest.deleteById(dummyId);

        //THEN
        verify(labelRepository).deleteById(dummyId);
    }

    @Test
    void testSave() {
        //GIVEN
        Long id = 1L;
        String name = "DUMMY - name";

        LabelDto input = LabelDto.builder().build();
        LabelDto expectedResult = LabelDto.builder().build();

        Label convertedLabelFromInput = Label.builder().build();
        Label savedLabel = Label.builder().build();

        when(labelMapper.convertToEntity(input)).thenReturn(convertedLabelFromInput);
        when(labelRepository.save(convertedLabelFromInput)).thenReturn(savedLabel);
        when(labelMapper.convertToDto(savedLabel)).thenReturn(expectedResult);

        //WHEN
        LabelDto result = underTest.save(input);

        //THEN
        assertEquals(expectedResult, result);
        verify(labelMapper).convertToEntity(input);
        verify(labelRepository).save(convertedLabelFromInput);
        verify(labelMapper).convertToDto(savedLabel);
    }

    @Test
    void shouldFindByIdReturnFoundLabels() {
        //GIVEN
        Long inputId = 1L;

        Label label = mock(Label.class);

        LabelDto expectedLabelDto = mock(LabelDto.class);

        when(labelRepository.findById(inputId)).thenReturn(Optional.of(label));
        when(labelMapper.convertToDto(label)).thenReturn(expectedLabelDto);

        //WHEN
        LabelDto result = underTest.findById(inputId);

        //THEN
        assertEquals(expectedLabelDto, result);
        verify(labelRepository).findById(inputId);
        verify(labelMapper).convertToDto(label);
    }

    @Test
    void shouldFindByIdThrowExceptionWhenNoData() {
        //GIVEN
        Long inputId = 1L;

        when(labelRepository.findById(inputId)).thenReturn(Optional.empty());

        //WHEN-THEN
        assertThrows(DataNotFoundException.class, () -> underTest.findById(inputId));
        verify(labelRepository).findById(inputId);
        verify(labelMapper, times(0)).convertToDto(any());
    }

    @Test
    void shouldFindAllRetrieveAllItems() {
        //GIVEN
        LabelDto labelDto = mock(LabelDto.class);
        Label label = mock(Label.class);

        when(labelRepository.findAll()).thenReturn(List.of(label));
        when(labelMapper.convertToDto(label)).thenReturn(labelDto);

        List<LabelDto> expectedResult = List.of(labelDto);

        //WHEN
        List<LabelDto> result = underTest.findAll();

        //THEN
        assertEquals(expectedResult, result);
    }

    @Test
    void testUpdateWithFoundObject() {
        //GIVEN
        Long id = 1L;
        LabelDto inputLabelDto = mock(LabelDto.class);
        Label label = mock(Label.class);

        when(inputLabelDto.getId()).thenReturn(id);
        when(labelRepository.findById(id)).thenReturn(Optional.of(label));
        doNothing().when(labelMapper).updateEntity(label, inputLabelDto);
        when(labelRepository.save(label)).thenReturn(label);
        when(labelMapper.convertToDto(label)).thenReturn(inputLabelDto);

        //WHEN
        LabelDto result = underTest.update(inputLabelDto);

        //THEN
        assertEquals(inputLabelDto, result);
        verify(labelRepository).findById(id);
        verify(labelMapper).updateEntity(label, inputLabelDto);
        verify(labelRepository).save(label);
        verify(labelMapper).convertToDto(label);
    }

    @Test
    void testUpdateWithoutObject() {
        //GIVEN
        Long id = 1L;
        LabelDto inputLabelDto = mock(LabelDto.class);

        when(inputLabelDto.getId()).thenReturn(id);
        when(labelRepository.findById(id)).thenReturn(Optional.empty());

        //WHEN-THEN
        assertThrows(DataNotFoundException.class, () -> underTest.update(inputLabelDto));
        verify(labelRepository).findById(id);
        verify(labelMapper, times(0)).updateEntity(any(), any());
        verify(labelRepository, times(0)).save(any());
        verify(labelMapper, times(0)).convertToDto(any());
    }
}