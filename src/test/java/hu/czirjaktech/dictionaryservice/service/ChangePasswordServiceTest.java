package hu.czirjaktech.dictionaryservice.service;

import hu.czirjaktech.dictionaryservice.dto.Role;
import hu.czirjaktech.dictionaryservice.dto.UserDto;
import hu.czirjaktech.dictionaryservice.dto.request.ChangePasswordRequest;
import hu.czirjaktech.dictionaryservice.exception.DataNotFoundException;
import hu.czirjaktech.dictionaryservice.mapper.UserMapper;
import hu.czirjaktech.dictionaryservice.repository.user.User;
import hu.czirjaktech.dictionaryservice.repository.user.UserRepository;
import hu.czirjaktech.dictionaryservice.security.Aes256Encoder;
import hu.czirjaktech.dictionaryservice.security.PrincipalService;
import hu.czirjaktech.dictionaryservice.validator.ChangePasswordRequestValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ValidationException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class ChangePasswordServiceTest {
    @Mock
    ChangePasswordRequestValidator changePasswordRequestValidator;

    @Mock
    UserRepository userRepository;

    @Mock
    UserMapper userMapper;

    @Mock
    PrincipalService principalService;

    @Mock
    Aes256Encoder passwordEncoder;

    @InjectMocks
    ChangePasswordService underTest;

    @Test
    void testChangePasswordMechanismWithHappyPath() {
        //Get
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
        changePasswordRequest.setCurrentPassword("Dummy123");
        changePasswordRequest.setNewPassword("NewDummy123");
        changePasswordRequest.setConfirmNewPassword("NewDummy123");

        Long id = 1L;
        Role role = Role.ROLE_USER;

        doNothing().when(changePasswordRequestValidator).validate(changePasswordRequest);

        User user = User.builder()
                .id(id)
                .password(changePasswordRequest.getCurrentPassword())
                .role(role)
                .active(true)
                .username("username@dummy.com")
                .build();

        User userWithNewPassword = User.builder()
                .id(id)
                .password(changePasswordRequest.getNewPassword())
                .role(role)
                .active(true)
                .username("username@dummy.com")
                .build();

        UserDto expectedResult = UserDto.builder()
                .id(id)
                .password(changePasswordRequest.getNewPassword())
                .role(role)
                .active(true)
                .username("username@dummy.com")
                .build();

        when(passwordEncoder.matches(changePasswordRequest.getCurrentPassword(), user.getPassword())).thenReturn(true);
        when(passwordEncoder.encode(changePasswordRequest.getNewPassword())).thenReturn(changePasswordRequest.getNewPassword());
        when(principalService.findCurrentUser()).thenReturn(Optional.of(user));
        when(userRepository.save(userWithNewPassword)).thenReturn(userWithNewPassword);
        when(userMapper.convertToDto(userWithNewPassword)).thenReturn(expectedResult);

        //When
        UserDto result = underTest.changePassword(changePasswordRequest);

        //Then
        assertEquals(expectedResult, result);
        verify(changePasswordRequestValidator).validate(changePasswordRequest);
        verify(passwordEncoder).encode(changePasswordRequest.getNewPassword());
        verify(principalService).findCurrentUser();
        verify(userRepository).save(userWithNewPassword);
        verify(userMapper).convertToDto(userWithNewPassword);
    }

    @Test
    void shouldThrowExceptionWhenNotHavingUser() {
        //Get
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
        changePasswordRequest.setCurrentPassword("Dummy123");
        changePasswordRequest.setNewPassword("NewDummy123");
        changePasswordRequest.setConfirmNewPassword("NewDummy123");

        Long id = 1L;
        Role role = Role.ROLE_USER;

        doNothing().when(changePasswordRequestValidator).validate(changePasswordRequest);

        User user = User.builder()
                .id(id)
                .password(changePasswordRequest.getCurrentPassword())
                .role(role)
                .active(true)
                .username("username@dummy.com")
                .build();

        User userWithNewPassword = User.builder()
                .id(id)
                .password(changePasswordRequest.getNewPassword())
                .role(role)
                .active(true)
                .username("username@dummy.com")
                .build();

        UserDto expectedResult = UserDto.builder()
                .id(id)
                .password(changePasswordRequest.getNewPassword())
                .role(role)
                .active(true)
                .username("username@dummy.com")
                .build();

        when(principalService.findCurrentUser()).thenReturn(Optional.empty());

        //When - Then
        assertThrows(DataNotFoundException.class, () -> underTest.changePassword(changePasswordRequest));
        verify(changePasswordRequestValidator).validate(changePasswordRequest);
        verify(passwordEncoder, times(0)).encode(changePasswordRequest.getCurrentPassword());
        verify(principalService).findCurrentUser();
        verify(userRepository, times(0)).save(userWithNewPassword);
        verify(userMapper, times(0)).convertToDto(userWithNewPassword);
    }

    @Test
    void shouldThrowExceptionWhenPasswordsNotMatching() {
        //Get
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
        changePasswordRequest.setCurrentPassword("Dummy123");
        changePasswordRequest.setNewPassword("NewDummy123");
        changePasswordRequest.setConfirmNewPassword("NewDummy123");

        Long id = 1L;
        Role role = Role.ROLE_USER;

        doNothing().when(changePasswordRequestValidator).validate(changePasswordRequest);

        User user = User.builder()
                .id(id)
                .password("Dummy123456")
                .role(role)
                .active(true)
                .username("username@dummy.com")
                .build();

        User userWithNewPassword = User.builder()
                .id(id)
                .password(changePasswordRequest.getCurrentPassword())
                .role(role)
                .active(true)
                .username("username@dummy.com")
                .build();

        UserDto expectedResult = UserDto.builder()
                .id(id)
                .password(changePasswordRequest.getNewPassword())
                .role(role)
                .active(true)
                .username("username@dummy.com")
                .build();

        when(principalService.findCurrentUser()).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(changePasswordRequest.getCurrentPassword(), user.getPassword())).thenReturn(false);

        //When - Then
        assertThrows(ValidationException.class, () -> underTest.changePassword(changePasswordRequest));
        verify(changePasswordRequestValidator).validate(changePasswordRequest);
        verify(passwordEncoder, times(0)).encode(changePasswordRequest.getCurrentPassword());
        verify(principalService).findCurrentUser();
        verify(userRepository, times(0)).save(userWithNewPassword);
        verify(userMapper, times(0)).convertToDto(userWithNewPassword);
    }
}