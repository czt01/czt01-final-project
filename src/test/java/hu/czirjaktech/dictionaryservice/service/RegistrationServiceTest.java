package hu.czirjaktech.dictionaryservice.service;

import hu.czirjaktech.dictionaryservice.dto.Role;
import hu.czirjaktech.dictionaryservice.dto.UserDto;
import hu.czirjaktech.dictionaryservice.dto.request.RegistrationRequest;
import hu.czirjaktech.dictionaryservice.exception.UserAlreadyDefinedException;
import hu.czirjaktech.dictionaryservice.mapper.UserMapper;
import hu.czirjaktech.dictionaryservice.repository.user.User;
import hu.czirjaktech.dictionaryservice.repository.user.UserRepository;
import hu.czirjaktech.dictionaryservice.validator.RegistrationRequestValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RegistrationServiceTest {

    @InjectMocks
    private RegistrationService underTest;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private UserMapper userMapper;

    @Mock
    private RegistrationRequestValidator registrationRequestValidator;

    @Test
    void testRegistrationHappyPath(){
        //Given
        Long id = 1L;
        Role role = Role.ROLE_USER;

        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setPassword("Dummy123");
        registrationRequest.setPasswordAgain("Dummy123");
        registrationRequest.setUsername("dummy@dummy.com");

        doNothing().when(registrationRequestValidator).validate(registrationRequest);
        when(passwordEncoder.encode(registrationRequest.getPassword())).thenReturn(registrationRequest.getPassword());
        when(userRepository.findByUsername(registrationRequest.getUsername())).thenReturn(Optional.empty());

        User newUser = User.builder()
                .active(true)
                .role(role)
                .username(registrationRequest.getUsername())
                .password(registrationRequest.getPassword())
                .build();

        User savedUser = User.builder()
                .id(id)
                .active(true)
                .role(role)
                .username(registrationRequest.getUsername())
                .password(registrationRequest.getPassword())
                .build();

        UserDto expectedResult = UserDto.builder()
                .id(id)
                .active(true)
                .role(role)
                .username(registrationRequest.getUsername())
                .password(registrationRequest.getPassword())
                .build();

        when(userRepository.save(newUser)).thenReturn(savedUser);
        when(userMapper.convertToDto(savedUser)).thenReturn(expectedResult);

        //When
        UserDto result = underTest.register(registrationRequest);

        //Then
        assertEquals(expectedResult, result);
        verify(registrationRequestValidator).validate(registrationRequest);
        verify(passwordEncoder).encode(registrationRequest.getPassword());
        verify(userRepository).findByUsername(registrationRequest.getUsername());
        verify(userRepository).save(newUser);
        verify(userMapper).convertToDto(savedUser);
    }

    @Test
    void testUserAlreadyDefinedException(){
        Long id = 1L;
        Role role = Role.ROLE_USER;

        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setPassword("Dummy123");
        registrationRequest.setPasswordAgain("Dummy123");
        registrationRequest.setUsername("dummy@dummy.com");

        doNothing().when(registrationRequestValidator).validate(registrationRequest);

        User newUser = User.builder()
                .active(true)
                .role(role)
                .username(registrationRequest.getUsername())
                .password(registrationRequest.getPassword())
                .build();

        User savedUser = User.builder()
                .id(id)
                .active(true)
                .role(role)
                .username(registrationRequest.getUsername())
                .password(registrationRequest.getPassword())
                .build();

        User registeringUser = User.builder()
                .id(id)
                .active(true)
                .role(role)
                .username(registrationRequest.getUsername())
                .password(registrationRequest.getPassword())
                .build();

        when(userRepository.findByUsername(registrationRequest.getUsername())).thenReturn(Optional.of(registeringUser));


        //When - Then
        Assertions.assertThrows(UserAlreadyDefinedException.class, () -> underTest.register(registrationRequest));
        verify(registrationRequestValidator).validate(registrationRequest);
        verify(passwordEncoder, times(0)).encode(registrationRequest.getPassword());
        verify(userRepository).findByUsername(registrationRequest.getUsername());
        verify(userRepository, times(0)).save(newUser);
        verify(userMapper, times(0)).convertToDto(savedUser);


    }
}

