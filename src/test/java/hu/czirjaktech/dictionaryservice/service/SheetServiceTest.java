package hu.czirjaktech.dictionaryservice.service;

import hu.czirjaktech.dictionaryservice.dto.LabelDto;
import hu.czirjaktech.dictionaryservice.dto.SheetDto;
import hu.czirjaktech.dictionaryservice.exception.DataNotFoundException;
import hu.czirjaktech.dictionaryservice.mapper.SheetMapper;
import hu.czirjaktech.dictionaryservice.repository.label.Label;
import hu.czirjaktech.dictionaryservice.repository.sheet.Sheet;
import hu.czirjaktech.dictionaryservice.repository.sheet.SheetRepository;
import hu.czirjaktech.dictionaryservice.security.PrincipalService;
import org.h2.command.dml.MergeUsing;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class SheetServiceTest {
    @Mock
    private SheetRepository sheetRepository;
    @Mock
    private PrincipalService principalService;
    @Mock
    private SheetMapper sheetMapper;
    @InjectMocks
    private SheetService underTest;

//    @Test
//    void testSave() {
//        //GIVEN
//        SheetDto expectedValue = SheetDto.builder().build();
//
//        Sheet sheet = Sheet.builder().build();
//
//        when(sheetMapper.convertToEntity(expectedValue)).thenReturn(sheet);
//        when(sheetRepository.save(sheet)).thenReturn(sheet);
//        when(sheetMapper.convertToDto(sheet)).thenReturn(expectedValue);
//
//        //WHEN
//        SheetDto result = underTest.save(expectedValue);
//
//        //THEN
//        assertEquals(expectedValue, result);
//        verify(sheetMapper).convertToEntity(expectedValue);
//        verify(sheetRepository).save(sheet);
//        verify(sheetMapper).convertToDto(sheet);
//    }

    @Test
    void testFindAll() {
        //GIVEN
        SheetDto sheetDto = SheetDto.builder().build();
        Sheet sheet = Sheet.builder().build();

        List<SheetDto> expectedResult = List.of(sheetDto);

        when(sheetRepository.findAll()).thenReturn(List.of(sheet));
        when(sheetMapper.convertToDto(sheet)).thenReturn(sheetDto);

        //WHEN
        List<SheetDto> result = underTest.findAll();

        //THEN
        assertEquals(expectedResult, result);
        verify(sheetRepository).findAll();
        verify(sheetMapper).convertToDto(sheet);
    }

    @Test
    void testFindByIdIfFoundSheet() {
        //GIVEN
        Long id = 1L;

        SheetDto expectedResult = SheetDto.builder().build();
        Sheet sheet = Sheet.builder().build();

        when(sheetRepository.findById(id)).thenReturn(Optional.of(sheet));
        when(sheetMapper.convertToDto(sheet)).thenReturn(expectedResult);

        //WHEN
        SheetDto result = underTest.findById(id);

        //THEN
        assertEquals(expectedResult, result);
        verify(sheetRepository).findById(id);
        verify(sheetMapper).convertToDto(sheet);
    }

    @Test
    void testFindByIdIfSheetNotFound() {
        //GIVEN
        Long id = 1L;

        when(sheetRepository.findById(id)).thenReturn(Optional.empty());

        //WHEN-THEN
        assertThrows(DataNotFoundException.class, () -> underTest.findById(id));
        verify(sheetRepository).findById(id);
        verify(sheetMapper, times(0)).convertToDto(any());
    }

    @Test
    void testDeleteById() {
        //GIVEN
        Long id = 1L;

        doNothing().when(sheetRepository).deleteById(id);

        //WHEN
        underTest.deleteById(id);

        //THEN
        verify(sheetRepository).deleteById(id);
    }
}