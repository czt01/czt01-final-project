package hu.czirjaktech.dictionaryservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LabelDto {

    @JsonProperty(value = "labelId")
    private Long id;

    @JsonProperty(value = "name")
    private String name;

}
