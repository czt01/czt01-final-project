package hu.czirjaktech.dictionaryservice.dto;

import hu.czirjaktech.dictionaryservice.repository.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SheetDto {

    private Long id;

    private String name;

    private String description;

    @Builder.Default
    private List<WordPairDto> wordPairs = new LinkedList<>();

    @Builder.Default
    private Set<LabelDto> labels = new HashSet<>();
}
