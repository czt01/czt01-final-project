package hu.czirjaktech.dictionaryservice.dto;

public enum Role {
        ROLE_ADMIN,
        ROLE_USER
}
