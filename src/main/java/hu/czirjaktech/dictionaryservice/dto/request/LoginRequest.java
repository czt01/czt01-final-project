package hu.czirjaktech.dictionaryservice.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginRequest {
    @NonNull
    @JsonProperty(value = "username")
    private String username;

    @NonNull
    @JsonProperty(value = "password")
    private String password;
}
