package hu.czirjaktech.dictionaryservice.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationRequest {

    private static final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";

    @Email
    @JsonProperty(value = "username")
    private String username;

    @Pattern(regexp = PASSWORD_PATTERN)
    @JsonProperty(value = "password")
    private String password;

    @Pattern(regexp = PASSWORD_PATTERN)
    @JsonProperty(value = "passwordAgain")
    private String passwordAgain;
}
