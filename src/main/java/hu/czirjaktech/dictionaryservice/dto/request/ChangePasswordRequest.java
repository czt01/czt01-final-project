package hu.czirjaktech.dictionaryservice.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChangePasswordRequest {

    @JsonProperty(value = "password")
    private String currentPassword;

    @JsonProperty(value = "newPassword")
    private String newPassword;

    @JsonProperty(value = "confirmNewPassword")
    private String confirmNewPassword;
}
