package hu.czirjaktech.dictionaryservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WordPairDto {

    private String leftValue;

    private String rightValue;

    private Long id;

}
