package hu.czirjaktech.dictionaryservice.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class PlatformResponse {

    @JsonProperty(value = "data")
    Object data;

    @JsonProperty(value = "success")
    boolean success;

    @JsonProperty(value = "error")
    String error;

    public PlatformResponse(Object data) {
        this.data = data;
        this.success = true;
        this.error = "";
    }

    public PlatformResponse(Throwable throwable) {
        this.data = null;
        this.success = false;
        this.error = throwable.getMessage();
    }
}
