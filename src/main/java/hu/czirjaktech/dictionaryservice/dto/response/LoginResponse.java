package hu.czirjaktech.dictionaryservice.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import hu.czirjaktech.dictionaryservice.dto.Role;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginResponse {
    @NonNull
    @JsonProperty(value="username")
    private String username;

    @NonNull
    @JsonProperty(value = "token")
    private String token;

    @NonNull
    @JsonProperty(value = "role")
    private Role role;

}
