package hu.czirjaktech.dictionaryservice.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.czirjaktech.dictionaryservice.dto.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationResponse {

    @JsonProperty(value = "username")
    private String username;

    @JsonProperty(value = "password")
    private String password;

    @JsonProperty(value = "role")
    private Role role;

    @JsonProperty(value = "active")
    private boolean active = true;
}
