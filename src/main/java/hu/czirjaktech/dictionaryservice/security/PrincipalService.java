package hu.czirjaktech.dictionaryservice.security;

import hu.czirjaktech.dictionaryservice.repository.user.User;
import hu.czirjaktech.dictionaryservice.repository.user.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PrincipalService {

    private final UserRepository userRepository;

    public PrincipalService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> findCurrentUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return Optional.empty();
        }
        org.springframework.security.core.userdetails.User springSecurityUser = (org.springframework.security.core.userdetails.User) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        return userRepository.findByUsername(springSecurityUser.getUsername());
    }
}
