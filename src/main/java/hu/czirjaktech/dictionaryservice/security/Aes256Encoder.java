package hu.czirjaktech.dictionaryservice.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.spec.KeySpec;
import java.util.Base64;

@Slf4j
@Component
public class Aes256Encoder implements PasswordEncoder {

    private static final String ALGORITHM = "AES";
    private static final String CIPHER_TYPE = "AES/CBC/PKCS5Padding";

    private final String password;
    private final String salt;
    private final Integer iteration;
    private final Integer keyLength;

    public Aes256Encoder(@Value("${dictionary.security.aes256-password-encoder.password}") String password,
                         @Value("${dictionary.security.aes256-password-encoder.salt}") String salt,
                         @Value("${dictionary.security.aes256-password-encoder.iteration}") Integer iteration,
                         @Value("${dictionary.security.aes256-password-encoder.key-length}") Integer keyLength) {
        this.password = password;
        this.salt = salt;
        this.iteration = iteration;
        this.keyLength = keyLength;
    }

    @Override
    public String encode(CharSequence cs) {
        try {
            String stringToEncrypt = cs.toString();
            IvParameterSpec ivSpec = new IvParameterSpec(new byte[16]);

            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(Pbkdf2PasswordEncoder.SecretKeyFactoryAlgorithm.PBKDF2WithHmacSHA256.name());
            KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), iteration, keyLength);
            SecretKey tmp = secretKeyFactory.generateSecret(keySpec);
            SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);

            Cipher cipher = Cipher.getInstance(CIPHER_TYPE);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);

            return Base64.getEncoder().encodeToString(cipher.doFinal(stringToEncrypt.getBytes(StandardCharsets.UTF_8)));

        } catch (Exception ex) {
            log.warn("Unable to encrypt value of {}", cs);
            throw new RuntimeException(ex);

        }
    }

    @Override
    public boolean matches(CharSequence cs, String string) {
        return encode(cs).equals(string);
    }
}
