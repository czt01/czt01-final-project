package hu.czirjaktech.dictionaryservice.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Slf4j
public class TimeAspect {
    @Around("@annotation(hu.czirjaktech.dictionaryservice.aop.annotation.TimeMeasurement)")
    public Object execute(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.nanoTime();

        Object methodResult = joinPoint.proceed();

        long end = System.nanoTime();

        log.info("{} was running for {} ms", joinPoint.getSignature().getName(), (end-start)/1_000_000);

        return methodResult;
    }
}
