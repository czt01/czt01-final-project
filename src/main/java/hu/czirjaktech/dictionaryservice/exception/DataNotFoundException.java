package hu.czirjaktech.dictionaryservice.exception;

public class DataNotFoundException extends RuntimeException {
    public DataNotFoundException(String e) {
        super(e);
    }
}
