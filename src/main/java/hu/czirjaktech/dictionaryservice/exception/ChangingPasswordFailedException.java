package hu.czirjaktech.dictionaryservice.exception;

public class ChangingPasswordFailedException extends RuntimeException {
    public ChangingPasswordFailedException(String e) {
        super(e);
    }
}
