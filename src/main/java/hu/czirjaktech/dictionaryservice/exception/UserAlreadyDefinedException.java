package hu.czirjaktech.dictionaryservice.exception;

public class UserAlreadyDefinedException extends RuntimeException {
    public UserAlreadyDefinedException(String name) {
        super("User is Already defined!" + name);
    }
}
