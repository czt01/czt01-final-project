package hu.czirjaktech.dictionaryservice.service;

import hu.czirjaktech.dictionaryservice.dto.LabelDto;
import hu.czirjaktech.dictionaryservice.exception.DataNotFoundException;
import hu.czirjaktech.dictionaryservice.mapper.LabelMapper;
import hu.czirjaktech.dictionaryservice.repository.label.Label;
import hu.czirjaktech.dictionaryservice.repository.label.LabelRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LabelService {
    private final LabelRepository labelRepository;
    private final LabelMapper labelMapper;

    public LabelService(LabelRepository labelRepository, LabelMapper labelMapper) {
        this.labelRepository = labelRepository;
        this.labelMapper = labelMapper;
    }

    public LabelDto save(LabelDto labelDto) {
        Label label = labelMapper.convertToEntity(labelDto);
        Label savedLabel = labelRepository.save(label);
        return labelMapper.convertToDto(savedLabel);
    }

    public LabelDto findById(Long id) {
        Optional<Label> labelOptional = labelRepository.findById(id);
        if (labelOptional.isPresent()) {
            return labelMapper.convertToDto(labelOptional.get());
        }
        throw new DataNotFoundException("Label not found");
    }

    public List<LabelDto> findAll() {
            return labelRepository.findAll()
                    .stream()
                    .map(labelMapper::convertToDto)
                    .collect(Collectors.toList());
    }

    public LabelDto update(LabelDto labelDto) {
        Optional<Label> existingLabel = labelRepository.findById(labelDto.getId());
        if(existingLabel.isPresent()) {
            Label label = existingLabel.get();
            labelMapper.updateEntity(label, labelDto);
            return labelMapper.convertToDto(labelRepository.save(label));
        }
        throw new DataNotFoundException("Label not found");
    }

    public void deleteAll() {
        labelRepository.deleteAll();
    }

    public void deleteById(Long id) {
        labelRepository.deleteById(id);
    }
}
