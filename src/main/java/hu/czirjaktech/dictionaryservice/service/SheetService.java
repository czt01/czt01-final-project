package hu.czirjaktech.dictionaryservice.service;

import hu.czirjaktech.dictionaryservice.dto.SheetDto;
import hu.czirjaktech.dictionaryservice.exception.DataNotFoundException;
import hu.czirjaktech.dictionaryservice.mapper.SheetMapper;
import hu.czirjaktech.dictionaryservice.repository.sheet.Sheet;
import hu.czirjaktech.dictionaryservice.repository.sheet.SheetRepository;
import hu.czirjaktech.dictionaryservice.repository.user.User;
import hu.czirjaktech.dictionaryservice.security.PrincipalService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SheetService {

    private final SheetRepository sheetRepository;
    private final SheetMapper sheetMapper;
    private final PrincipalService principalService;

    public SheetService(SheetRepository sheetRepository, SheetMapper sheetMapper, PrincipalService principalService) {
        this.sheetRepository = sheetRepository;
        this.sheetMapper = sheetMapper;
        this.principalService = principalService;
    }

    public SheetDto save(SheetDto sheetDto) {
        Sheet sheet = sheetMapper.convertToEntity(sheetDto);
        Optional<User> userPrincipalOptional = principalService.findCurrentUser();
        if(userPrincipalOptional.isPresent()) {
            sheet.setUser(userPrincipalOptional.get());
            return sheetMapper.convertToDto(sheetRepository.save(sheet));
        }
        throw new DataNotFoundException("User not found");
    }

    public List<SheetDto> findAll() {
        return sheetRepository.findAll()
                .stream()
                .map(sheetMapper::convertToDto)
                .collect(Collectors.toList());
    }

    public SheetDto findById(Long id) {
        Optional<Sheet> sheetOptional = sheetRepository.findById(id);
        if (sheetOptional.isPresent()) {
            return sheetMapper.convertToDto(sheetOptional.get());
        }
        throw new DataNotFoundException("Sheet not found");
    }

    public void deleteById(Long id) {
        sheetRepository.deleteById(id);
    }
}
