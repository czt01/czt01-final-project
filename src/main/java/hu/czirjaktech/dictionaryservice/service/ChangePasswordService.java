package hu.czirjaktech.dictionaryservice.service;

import hu.czirjaktech.dictionaryservice.dto.UserDto;
import hu.czirjaktech.dictionaryservice.dto.request.ChangePasswordRequest;
import hu.czirjaktech.dictionaryservice.exception.DataNotFoundException;
import hu.czirjaktech.dictionaryservice.mapper.UserMapper;
import hu.czirjaktech.dictionaryservice.repository.user.User;
import hu.czirjaktech.dictionaryservice.repository.user.UserRepository;
import hu.czirjaktech.dictionaryservice.security.Aes256Encoder;
import hu.czirjaktech.dictionaryservice.security.PrincipalService;
import hu.czirjaktech.dictionaryservice.validator.ChangePasswordRequestValidator;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.Optional;

@Service
public class ChangePasswordService {

    private final ChangePasswordRequestValidator changePasswordRequestValidator;
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PrincipalService principalService;
    private final Aes256Encoder passwordEncoder;

    public ChangePasswordService(ChangePasswordRequestValidator changePasswordRequestValidator,
                                 UserRepository userRepository,
                                 UserMapper userMapper,
                                 PrincipalService principalService,
                                 Aes256Encoder passwordEncoder) {
        this.changePasswordRequestValidator = changePasswordRequestValidator;
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.principalService = principalService;
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto changePassword(ChangePasswordRequest changePasswordRequest) {
        changePasswordRequestValidator.validate(changePasswordRequest);

        Optional<User> userOptional = principalService.findCurrentUser();

        if (userOptional.isEmpty()) {
            throw new DataNotFoundException("User is not registered");
        }
        User user = userOptional.get();

        if (!passwordEncoder.matches(changePasswordRequest.getCurrentPassword(), user.getPassword())) {
            throw new ValidationException("Passwords must be the same");
        }

        user.setPassword(passwordEncoder.encode(changePasswordRequest.getNewPassword()));
        userRepository.save(user);

        return userMapper.convertToDto(user);
    }
}

