package hu.czirjaktech.dictionaryservice.service;

import hu.czirjaktech.dictionaryservice.dto.WordPairDto;
import hu.czirjaktech.dictionaryservice.exception.DataNotFoundException;
import hu.czirjaktech.dictionaryservice.mapper.WordPairMapper;
import hu.czirjaktech.dictionaryservice.repository.sheet.Sheet;
import hu.czirjaktech.dictionaryservice.repository.sheet.SheetRepository;
import hu.czirjaktech.dictionaryservice.repository.wordpair.WordPair;
import hu.czirjaktech.dictionaryservice.repository.wordpair.WordPairRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WordpairService {
    private final WordPairRepository wordPairRepository;
    private final SheetRepository sheetRepository;
    private final WordPairMapper wordPairMapper;

    public WordpairService(WordPairRepository wordPairRepository, SheetRepository sheetRepository, WordPairMapper wordPairMapper) {
        this.wordPairRepository = wordPairRepository;
        this.sheetRepository = sheetRepository;
        this.wordPairMapper = wordPairMapper;
    }

    public WordPairDto save(WordPairDto wordPairDto, Long sheetId) {
        Optional<Sheet> sheetOptional = sheetRepository.findById(sheetId);
        if(sheetOptional.isPresent()) {
            WordPair wordPair = wordPairMapper.convertToEntity(wordPairDto);
            wordPair.setSheet(sheetOptional.get());
            return wordPairMapper.convertToDto(wordPairRepository.save(wordPair));
        }
        throw new DataNotFoundException("Sheet not found");
    }

    public WordPairDto update(WordPairDto wordPairDto) {
        Optional<WordPair> existingWordPair = wordPairRepository.findById(wordPairDto.getId());
        if(existingWordPair.isPresent()) {
            WordPair wordPair = existingWordPair.get();
            wordPairMapper.updateEntity(wordPair, wordPairDto);
            return wordPairMapper.convertToDto(wordPairRepository.save(wordPair));
        }
        throw new DataNotFoundException("Item not found");
    }

    public List<WordPairDto> findAll() {
        return wordPairRepository.findAll()
                .stream()
                .map(wordPairMapper::convertToDto)
                .collect(Collectors.toList());
    }

    public WordPairDto findById(Long id) {
        Optional<WordPair> wordPairOptional = wordPairRepository.findById(id);
        if(wordPairOptional.isPresent()) {
            return wordPairMapper.convertToDto(wordPairOptional.get());
        }
        throw new DataNotFoundException("Wordpair not found");
    }

    public void deleteAll() {
        wordPairRepository.deleteAll();
    }

    public void deleteById(Long id) {
        wordPairRepository.deleteById(id);
    }
}
