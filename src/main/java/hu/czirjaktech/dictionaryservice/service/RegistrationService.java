package hu.czirjaktech.dictionaryservice.service;

import hu.czirjaktech.dictionaryservice.aop.annotation.TimeMeasurement;
import hu.czirjaktech.dictionaryservice.dto.UserDto;
import hu.czirjaktech.dictionaryservice.dto.request.RegistrationRequest;
import hu.czirjaktech.dictionaryservice.exception.UserAlreadyDefinedException;
import hu.czirjaktech.dictionaryservice.mapper.UserMapper;
import hu.czirjaktech.dictionaryservice.repository.user.User;
import hu.czirjaktech.dictionaryservice.repository.user.UserRepository;
import hu.czirjaktech.dictionaryservice.validator.RegistrationRequestValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class RegistrationService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RegistrationRequestValidator registrationRequestValidator;

    public RegistrationService(UserMapper userMapper,
                               UserRepository userRepository,
                               PasswordEncoder passwordEncoder,
                               RegistrationRequestValidator registrationRequestValidator) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.registrationRequestValidator = registrationRequestValidator;
    }

    @TimeMeasurement
    public UserDto register(RegistrationRequest registrationRequest)  {

        registrationRequestValidator.validate(registrationRequest);

        Optional<User> userEntity = userRepository.findByUsername(registrationRequest.getUsername());

        if (userEntity.isPresent()){
            throw new UserAlreadyDefinedException(registrationRequest.getUsername());
        }

           User user = User.builder()
                   .username(registrationRequest.getUsername())
                   .password(passwordEncoder.encode(registrationRequest.getPassword()))
                   .build();

       User savedUser = userRepository.save(user);

        log.info("A new user has been saved {}", registrationRequest.getUsername());

        return userMapper.convertToDto(savedUser);
    }
}
