package hu.czirjaktech.dictionaryservice.repository.label;

import hu.czirjaktech.dictionaryservice.repository.sheet.Sheet;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "labels")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Label {

    @Id
    @Column(name = "labelId")
    @GeneratedValue
    @EqualsAndHashCode.Include
    private Long id;

    @Column(name = "name")
    private String name;

    @Builder.Default
    @ManyToMany(mappedBy = "labels", fetch = FetchType.LAZY)
    private Set<Sheet> sheets = new HashSet<>();
}
