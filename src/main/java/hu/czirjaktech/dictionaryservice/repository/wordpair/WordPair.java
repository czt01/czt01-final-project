package hu.czirjaktech.dictionaryservice.repository.wordpair;

import hu.czirjaktech.dictionaryservice.repository.sheet.Sheet;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "wordPairs")
public class WordPair {

    @Id
    @Column(name = "wordPairId", nullable = false)
    @EqualsAndHashCode.Include
    @GeneratedValue
    private Long id;

    @Column(name = "leftValue")
    private String leftValue;

    @Column(name = "rightValue")
    private String rightValue;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sheetId", nullable = false)
    private Sheet sheet;
}
