package hu.czirjaktech.dictionaryservice.repository.wordpair;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordPairRepository extends JpaRepository<WordPair,Long> {
}
