package hu.czirjaktech.dictionaryservice.repository.topic;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table (name="topics")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Topic {
    @Id
    @Column(name = "topicId")
    @EqualsAndHashCode.Include
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;
}
