package hu.czirjaktech.dictionaryservice.repository.user;

import hu.czirjaktech.dictionaryservice.dto.Role;
import hu.czirjaktech.dictionaryservice.repository.sheet.Sheet;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Builder
public class User {
    @Id
    @Column(name = "userId")
    @EqualsAndHashCode.Include
    @GeneratedValue
    private Long id;

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "password")
    private String password;

    @Builder.Default
    @Column(name = "role")
    private Role role = Role.ROLE_USER;

    @Builder.Default
    @Column(name = "active")
    private boolean active = true;

    @Builder.Default
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<Sheet> sheets = new ArrayList<>();
}
