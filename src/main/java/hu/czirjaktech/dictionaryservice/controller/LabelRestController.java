package hu.czirjaktech.dictionaryservice.controller;

import hu.czirjaktech.dictionaryservice.dto.LabelDto;

import hu.czirjaktech.dictionaryservice.dto.response.PlatformResponse;
import hu.czirjaktech.dictionaryservice.service.LabelService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
//TODO: Handle exceptions, errors
@RestController
public class LabelRestController {
    private static final String BASE_URL = "/labels";
    private static final String BASE_URL_WITH_ID = BASE_URL + "/{id}";
    private final LabelService labelService;

    public LabelRestController(LabelService labelService) {
        this.labelService = labelService;
    }

    @RequestMapping(path = BASE_URL, method = RequestMethod.POST)
    public ResponseEntity<PlatformResponse> save(@RequestBody LabelDto labelDto) throws URISyntaxException {
        LabelDto savedLabelDto = labelService.save(labelDto);
        return ResponseEntity.created(new URI(BASE_URL + "/" + savedLabelDto.getId())).body(new PlatformResponse(savedLabelDto));
    }

    @RequestMapping(path = BASE_URL_WITH_ID, method = RequestMethod.GET)
    public ResponseEntity<PlatformResponse> getById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(new PlatformResponse(labelService.findById(id)));
    }

    @RequestMapping(path = BASE_URL, method = RequestMethod.GET)
    public ResponseEntity<PlatformResponse> getAll() {
        return ResponseEntity.ok(new PlatformResponse(labelService.findAll()));
    }

    @RequestMapping(path = BASE_URL_WITH_ID, method = RequestMethod.PUT)
    public ResponseEntity<PlatformResponse> update(@RequestBody LabelDto labelDto) {
        return ResponseEntity.ok(new PlatformResponse(labelService.update(labelDto)));
    }

    @RequestMapping(path = BASE_URL, method = RequestMethod.DELETE)
    public ResponseEntity<PlatformResponse> deleteAll() {
        labelService.deleteAll();
        return ResponseEntity.ok(new PlatformResponse("OK"));
    }

    @RequestMapping(path = BASE_URL_WITH_ID, method = RequestMethod.DELETE)
    public ResponseEntity<PlatformResponse> deleteById(@PathVariable(name = "id") Long id) {
        labelService.deleteById(id);
        return ResponseEntity.ok(new PlatformResponse("OK"));
    }
}
