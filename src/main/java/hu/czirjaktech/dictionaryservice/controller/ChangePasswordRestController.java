package hu.czirjaktech.dictionaryservice.controller;

import hu.czirjaktech.dictionaryservice.dto.request.ChangePasswordRequest;
import hu.czirjaktech.dictionaryservice.dto.response.PlatformResponse;
import hu.czirjaktech.dictionaryservice.service.ChangePasswordService;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ChangePasswordRestController {

    private static final String CHANGE = "change";

    private ChangePasswordService changePasswordService;

    public ChangePasswordRestController(ChangePasswordService changePasswordService) {
        this.changePasswordService = changePasswordService;
    }

    //password?action=change
    @RequestMapping(path = "/password", method = RequestMethod.POST)
    public ResponseEntity<PlatformResponse> passwordChange(@RequestBody ChangePasswordRequest changePasswordRequest,
                                                           @RequestParam(value = "action") String action){

        if(CHANGE.equals(action)){
            return ResponseEntity.ok().body(new PlatformResponse(changePasswordService.changePassword(changePasswordRequest)));
        } else{
            return ResponseEntity.badRequest().body(new PlatformResponse(new IllegalArgumentException("Unsupported action type")));
        }
    }
}
