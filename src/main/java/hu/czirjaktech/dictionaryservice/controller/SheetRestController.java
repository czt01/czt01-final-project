package hu.czirjaktech.dictionaryservice.controller;

import hu.czirjaktech.dictionaryservice.dto.SheetDto;
import hu.czirjaktech.dictionaryservice.dto.response.PlatformResponse;
import hu.czirjaktech.dictionaryservice.service.SheetService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SheetRestController {
    private static final String URL = "/sheets";
    private static final String URL_WITH_ID = URL + "/{id}";

    private final SheetService sheetService;

    public SheetRestController(SheetService sheetService) {
        this.sheetService = sheetService;
    }

    @RequestMapping(path = URL, method = RequestMethod.POST)
    public ResponseEntity<PlatformResponse> save(@RequestBody SheetDto sheetDto) {
        return ResponseEntity.ok(new PlatformResponse(sheetService.save(sheetDto)));
    }

    @RequestMapping(path = URL, method = RequestMethod.GET)
    public ResponseEntity<PlatformResponse> findAll() {
        return ResponseEntity.ok(new PlatformResponse(sheetService.findAll()));
    }

    @RequestMapping(path = URL_WITH_ID, method = RequestMethod.GET)
    public ResponseEntity<PlatformResponse> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(new PlatformResponse(sheetService.findById(id)));
    }

    @RequestMapping(path = URL_WITH_ID, method = RequestMethod.DELETE)
    public ResponseEntity<PlatformResponse> delete(@PathVariable("id") Long id) {
        sheetService.deleteById(id);
        return ResponseEntity.ok(new PlatformResponse("OK"));
    }
}