package hu.czirjaktech.dictionaryservice.controller;

import hu.czirjaktech.dictionaryservice.dto.AuthenticatedUserDetails;
import hu.czirjaktech.dictionaryservice.dto.request.LoginRequest;
import hu.czirjaktech.dictionaryservice.dto.response.LoginResponse;
import hu.czirjaktech.dictionaryservice.dto.response.PlatformResponse;
import hu.czirjaktech.dictionaryservice.security.JwtTokenProvider;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class AuthenticationRestController {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider tokenProvider;

    public AuthenticationRestController(AuthenticationManager authenticationManager, JwtTokenProvider tokenProvider) {
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
    }

    @Operation(summary = "Login as a User")
    @PostMapping(path = "/login")
    public ResponseEntity<PlatformResponse> login(@RequestBody LoginRequest loginRequest) {
        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
            String token = tokenProvider.createToken(authentication);
            AuthenticatedUserDetails user = (AuthenticatedUserDetails) authentication.getPrincipal();

            LoginResponse loginResponse = LoginResponse.builder()
                    .username(user.getUsername())
                    .role(user.getRole())
                    .token(token)
                    .build();

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + loginResponse.getToken());

            return new ResponseEntity<>(new PlatformResponse(loginResponse), httpHeaders, HttpStatus.OK);

        } catch (Throwable throwable) {
            log.warn("Login failed {}", loginRequest.getUsername());
            return ResponseEntity
                    .status(HttpStatus.NOT_ACCEPTABLE)
                    .body(new PlatformResponse(new RuntimeException("Login failed")));
        }

    }
}
