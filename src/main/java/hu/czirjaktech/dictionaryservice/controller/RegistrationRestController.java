package hu.czirjaktech.dictionaryservice.controller;

import hu.czirjaktech.dictionaryservice.dto.UserDto;
import hu.czirjaktech.dictionaryservice.dto.request.RegistrationRequest;
import hu.czirjaktech.dictionaryservice.dto.response.PlatformResponse;
import hu.czirjaktech.dictionaryservice.service.RegistrationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
public class RegistrationRestController {

    private RegistrationService registrationService;

    public RegistrationRestController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @RequestMapping(path = "/registration", method = RequestMethod.POST)
    public ResponseEntity<PlatformResponse> register(@Valid @RequestBody RegistrationRequest registrationRequest) throws URISyntaxException {
        UserDto userDto = registrationService.register(registrationRequest);
        return ResponseEntity.created(new URI("/users/" + userDto.getId())).body(new PlatformResponse(userDto));
    }
}
