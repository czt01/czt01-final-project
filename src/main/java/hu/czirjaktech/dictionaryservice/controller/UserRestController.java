package hu.czirjaktech.dictionaryservice.controller;

import hu.czirjaktech.dictionaryservice.dto.UserDto;
import hu.czirjaktech.dictionaryservice.dto.response.PlatformResponse;
import hu.czirjaktech.dictionaryservice.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserRestController {

    private static final String BASE_URL = "/users";
    private static final String BASE_URL_WITH_ID = BASE_URL + "/{id}";
    private final UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(path = BASE_URL_WITH_ID, method = RequestMethod.DELETE)
    public ResponseEntity<PlatformResponse> deleteById(@PathVariable(name = "id") Long id) {
        UserDto userDto = userService.makeInactive(id);
        return ResponseEntity.ok(new PlatformResponse(userDto));
    }
}
