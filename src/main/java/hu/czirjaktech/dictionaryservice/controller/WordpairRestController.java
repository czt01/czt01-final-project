package hu.czirjaktech.dictionaryservice.controller;

import hu.czirjaktech.dictionaryservice.dto.WordPairDto;
import hu.czirjaktech.dictionaryservice.dto.response.PlatformResponse;
import hu.czirjaktech.dictionaryservice.service.WordpairService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class WordpairRestController {
    private static final String URL = "/words";
    private static final String URL_WITH_ID = URL + "/{id}";
    private static final String URL_WITH_SHEET_ID = URL + "/sheets/{id}";
    private final WordpairService wordpairRestService;

    public WordpairRestController(WordpairService wordpairRestService) {
        this.wordpairRestService = wordpairRestService;
    }

    @RequestMapping(path = URL_WITH_SHEET_ID, method = RequestMethod.POST)
    public ResponseEntity<PlatformResponse> save(@RequestBody WordPairDto wordPairDto, @PathVariable(name = "id") Long sheetId) {

        return ResponseEntity.ok(new PlatformResponse(wordpairRestService.save(wordPairDto, sheetId)));
    }

    @RequestMapping(path = URL, method = RequestMethod.GET)
    public ResponseEntity<PlatformResponse> findAll() {
        return ResponseEntity.ok(new PlatformResponse(wordpairRestService.findAll()));
    }

    @RequestMapping(path = URL_WITH_ID, method = RequestMethod.GET)
    public ResponseEntity<PlatformResponse> findById(@PathVariable(name= "id") Long id) {
        return ResponseEntity.ok(new PlatformResponse(wordpairRestService.findById(id)));
    }

    @RequestMapping(path = URL_WITH_ID, method = RequestMethod.PUT)
    public ResponseEntity<PlatformResponse> update(@RequestBody WordPairDto wordPairDto) {
        return ResponseEntity.ok(new PlatformResponse(wordpairRestService.update(wordPairDto)));
    }

    @RequestMapping(path = URL, method = RequestMethod.DELETE)
    public ResponseEntity<PlatformResponse> deleteAll() {
        wordpairRestService.deleteAll();
        return ResponseEntity.ok(new PlatformResponse("OK"));
    }

    @RequestMapping(path = URL_WITH_ID, method = RequestMethod.DELETE)
    public ResponseEntity<PlatformResponse> deleteById(@PathVariable(name = "id") Long id) {
        wordpairRestService.deleteById(id);
        return ResponseEntity.ok(new PlatformResponse("OK"));
    }
}
