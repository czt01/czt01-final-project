package hu.czirjaktech.dictionaryservice.validator;

import hu.czirjaktech.dictionaryservice.dto.request.ChangePasswordRequest;
import org.springframework.stereotype.Component;

import javax.validation.ValidationException;

@Component
public class ChangePasswordRequestValidator implements Validator<ChangePasswordRequest> {

    @Override
    public void validate(ChangePasswordRequest changePasswordRequest){
        if (!changePasswordRequest.getNewPassword().equals(changePasswordRequest.getConfirmNewPassword())){
            throw new ValidationException("Passwords must be the same");
        }
    }
}
