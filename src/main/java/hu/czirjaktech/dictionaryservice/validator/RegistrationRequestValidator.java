package hu.czirjaktech.dictionaryservice.validator;

import hu.czirjaktech.dictionaryservice.dto.request.RegistrationRequest;
import org.springframework.stereotype.Component;

import javax.validation.ValidationException;

@Component
public class RegistrationRequestValidator implements Validator<RegistrationRequest> {

    @Override
    public void validate(RegistrationRequest registrationRequest){
        if (!registrationRequest.getPassword().equals(registrationRequest.getPasswordAgain())){
            throw new ValidationException("Passwords must be the same");
        }
    }
}
