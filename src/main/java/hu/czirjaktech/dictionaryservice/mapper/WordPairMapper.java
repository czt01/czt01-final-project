package hu.czirjaktech.dictionaryservice.mapper;

import hu.czirjaktech.dictionaryservice.dto.WordPairDto;
import hu.czirjaktech.dictionaryservice.repository.wordpair.WordPair;
import org.springframework.stereotype.Component;

@Component
public class WordPairMapper implements Mapper<WordPair,WordPairDto> {

    @Override
    public WordPair convertToEntity(WordPairDto wordPairDto) {

        return WordPair.builder()
                .id(wordPairDto.getId())
                .leftValue(wordPairDto.getLeftValue())
                .rightValue(wordPairDto.getRightValue())
                .build();
    }

    @Override
    public WordPairDto convertToDto(WordPair wordPairEntity) {
        return WordPairDto.builder()
                .id(wordPairEntity.getId())
                .leftValue(wordPairEntity.getLeftValue())
                .rightValue(wordPairEntity.getRightValue())
                .build();
    }

    public void updateEntity(WordPair wordPair, WordPairDto wordPairDto) {
        wordPair.setLeftValue(wordPairDto.getLeftValue());
        wordPair.setRightValue(wordPairDto.getRightValue());
    }
}
