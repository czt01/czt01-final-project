package hu.czirjaktech.dictionaryservice.mapper;

public interface Mapper<Entity, Dto> {

    Entity convertToEntity(Dto dto);

    Dto convertToDto(Entity entity);
}
