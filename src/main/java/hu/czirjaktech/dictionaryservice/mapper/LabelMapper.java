package hu.czirjaktech.dictionaryservice.mapper;

import hu.czirjaktech.dictionaryservice.dto.LabelDto;
import hu.czirjaktech.dictionaryservice.repository.label.Label;
import org.springframework.stereotype.Component;

@Component
public class LabelMapper implements Mapper<Label,LabelDto>{

    @Override
    public LabelDto convertToDto(Label label) {
        return LabelDto
                .builder()
                .id(label.getId())
                .name(label.getName())
                .build();
    }

    @Override
    public Label convertToEntity(LabelDto labelDto) {
        return Label
                .builder()
                .id(labelDto.getId())
                .name(labelDto.getName())
                .build();
    }

    public void updateEntity(Label label, LabelDto labelDto) {
        label.setName(labelDto.getName());
    }
}
