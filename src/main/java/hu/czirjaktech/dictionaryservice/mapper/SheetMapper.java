package hu.czirjaktech.dictionaryservice.mapper;

import hu.czirjaktech.dictionaryservice.dto.SheetDto;
import hu.czirjaktech.dictionaryservice.repository.sheet.Sheet;
import hu.czirjaktech.dictionaryservice.repository.wordpair.WordPair;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;


@Component
public class SheetMapper implements Mapper<Sheet, SheetDto>{

    private final LabelMapper labelMapper;
    private final WordPairMapper wordPairMapper;

    public SheetMapper(LabelMapper labelMapper, WordPairMapper wordPairMapper) {
        this.labelMapper = labelMapper;
        this.wordPairMapper = wordPairMapper;
    }

    @Override
    public Sheet convertToEntity(SheetDto sheetDto) {
        return Sheet.builder()
                .id(sheetDto.getId())
                .description(sheetDto.getDescription())
                .name(sheetDto.getName())
                .labels(sheetDto.getLabels().stream().map(labelMapper::convertToEntity).collect(Collectors.toSet()))
                .wordPairs(sheetDto.getWordPairs().stream().map(wordPairMapper::convertToEntity).collect(Collectors.toList()))
                .build();
    }

    @Override
    public SheetDto convertToDto(Sheet sheetEntity) {
       return SheetDto.builder()
               .description(sheetEntity.getDescription())
               .id(sheetEntity.getId())
               .name(sheetEntity.getName())
               .labels(sheetEntity.getLabels().stream().map(labelMapper::convertToDto).collect(Collectors.toSet()))
               .wordPairs(sheetEntity.getWordPairs().stream().map(wordPairMapper::convertToDto).collect(Collectors.toList()))
               .build();
    }
}
