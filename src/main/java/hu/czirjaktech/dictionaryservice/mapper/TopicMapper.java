package hu.czirjaktech.dictionaryservice.mapper;

import hu.czirjaktech.dictionaryservice.dto.TopicDto;
import hu.czirjaktech.dictionaryservice.repository.topic.Topic;
import org.springframework.stereotype.Component;


@Component
public class TopicMapper implements Mapper<Topic, TopicDto>{

    @Override
    public Topic convertToEntity(TopicDto topicDto) {
        return Topic
                .builder()
                .id(topicDto.getId())
                .name(topicDto.getName())
                .description(topicDto.getDescription())
                .build();
    }

    @Override
    public TopicDto convertToDto(Topic topic) {
        return TopicDto
                .builder()
                .id(topic.getId())
                .name(topic.getName())
                .description(topic.getDescription())
                .build();
    }
}
