package hu.czirjaktech.dictionaryservice.mapper;

import hu.czirjaktech.dictionaryservice.dto.UserDto;
import hu.czirjaktech.dictionaryservice.repository.user.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper implements Mapper<User, UserDto>{

    @Override
    public User convertToEntity(UserDto userDto) {
        return User.builder()
                .username(userDto.getUsername())
                .role(userDto.getRole())
                .id(userDto.getId())
                .password(userDto.getPassword())
                .active(userDto.isActive())
                .build();
    }

    @Override
    public UserDto convertToDto(User user) {
        return UserDto.builder()
                .username(user.getUsername())
                .active(user.isActive())
                .id(user.getId())
                .password(user.getPassword())
                .role(user.getRole())
                .build();
    }
}
